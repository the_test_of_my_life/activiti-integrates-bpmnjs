package com.imooc.activitiweb.controller;

import com.imooc.activitiweb.mapper.UserInfoBeanMapper;
import com.imooc.activitiweb.util.AjaxResponse;
import com.imooc.activitiweb.util.GlobalConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserInfoBeanMapper userInfoBeanMapper;

    /**
     * 获取用户列表
     * @return
     */
    @GetMapping("/getUsers")
    public AjaxResponse getUsers(){
        try {
            // 获取用户列表
            List<Map<String,Object>> users = userInfoBeanMapper.selectUsers();
            return AjaxResponse.AjaxData(
                    GlobalConfig.ResponseCode.SUCCESS.getCode(),
                    GlobalConfig.ResponseCode.SUCCESS.getDesc(),
                    users
            );
        } catch (Exception e) {
            return AjaxResponse.AjaxData(
                    GlobalConfig.ResponseCode.ERROR.getCode(),
                    "获取用户列表失败",
                    e.toString()
            );
        }
    }
}
