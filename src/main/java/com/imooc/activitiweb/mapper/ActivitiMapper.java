package com.imooc.activitiweb.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Mapper
@Component
public interface ActivitiMapper {

    /**
     * 插入表单数
     * @param maps
     * @return
     */
    @Insert("<script>" +
            "insert into formdata(PROC_DEF_ID_,PROC_INST_ID_,FORM_KEY_,Control_ID_,Control_VALUE_)" +
            "values" +
            "<foreach collection=\"maps\" item=\"formdata\" index=\"index\" separator=\",\">" +
            "(" +
            "#{formdata.PROC_DEF_ID_,jdbcType=VARCHAR}," +
            "#{formdata.PROC_INST_ID_,jdbcType=VARCHAR}," +
            "#{formdata.FORM_KEY_,jdbcType=VARCHAR}," +
            "#{formdata.Control_ID_,jdbcType=VARCHAR}," +
            "#{formdata.Control_VALUE_,jdbcType=VARCHAR}" +
            ")" +
            "</foreach>" +
            "</script>")
    public int insertFormData(@Param("maps") List<Map<String,Object>> maps);

    /**
     * 读取表单数据
     * @param PROC_INST_ID   流程实例 id
     * @return
     */
    @Select("select Control_ID_,Control_VALUE_ from formdata where PROC_INST_ID_ = #{PROC_INST_ID}")
    public List<Map<String,String>> selectFormData(@Param("PROC_INST_ID")String PROC_INST_ID);
}
