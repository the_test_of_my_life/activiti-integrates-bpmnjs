package com.imooc.activitiweb.mapper;

import com.imooc.activitiweb.pojo.UserInfoBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

@Mapper
public interface UserInfoBeanMapper {

    /**
     * 根据用户名查询用户信息
     * @param username
     * @return
     */
    @Select("select * from user where username = #{username}")
    public UserInfoBean selectByUsername(@Param("username")String username);

    /**
     * 获取用户列表
     * @return
     */
    @Select("select name,username from user")
    public List<Map<String,Object>> selectUsers();
}
