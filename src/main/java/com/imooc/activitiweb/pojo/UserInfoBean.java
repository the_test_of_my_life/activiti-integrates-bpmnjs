package com.imooc.activitiweb.pojo;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * 用户类
 */
public class UserInfoBean implements UserDetails {

    private Long id;             // 编号
    private String name;         // 名字
    private String address;     // 地址
    private String username;    // 用户名
    private String password;    // 密码
    private String roles;       // 角色

    public String getAddress() {
        return address;
    }

    // 这里返回用户的角色列表,为角色赋值
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Arrays.
                stream(roles.split(",")).    // 使用 , 号分割角色字符串，引用数据库里是这么存的
                map(r -> new SimpleGrantedAuthority(r))
                .collect(Collectors.toList());
    }
    @Override
    public String getPassword() {
        return password;
    }
    @Override
    public String getUsername() {
        return username;
    }
    // 下面暂时都先返回 true
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
