package com.imooc.activitiweb.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * SpringSecurity 配置类
 */
@Configuration
public class ActivitiSercurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private LoginSuccessHandler loginSuccessHandler;
    @Autowired
    private LoginFailureHandler loginFailureHandler;
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
                .formLogin() // 这里我们就要使用 http 进行登录了，不能使用登录页登录了
                .loginPage("/login")  // 登录方法
                .loginProcessingUrl("/login")  // 配置没有访问权限的 url 的处理链接，需要 loginPage 一样
                .successHandler(loginSuccessHandler) // 登录成功处理类
                .failureHandler(loginFailureHandler) // 失败处理
                .and()
                .authorizeRequests()
                .antMatchers("/processDefinition/**","/bpmn/**","/login","/bpmnjs/**").permitAll() // 配置没有访问权限的 url 不需要登录访问
                .anyRequest().authenticated()  // 所有的 url 都需要登录才能访问
                .and()
                .logout().permitAll()
                .and()
                .csrf().disable()
                .headers().frameOptions().disable(); // frame 框架校验关闭，否则前端使用的 frame 框架也会报错
    }
}
