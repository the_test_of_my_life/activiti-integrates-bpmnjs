package com.imooc.activitiweb.util;

/**
 * ajax 返回类
 */
public class AjaxResponse {

    private int status;   // 返回状态码
    private String msg;    // 成功或错误提示
    private Object obj;    // 返回内容

    private AjaxResponse(int status, String msg, Object obj) {
        this.status = status;
        this.msg = msg;
        this.obj = obj;
    }
    public static AjaxResponse AjaxData(int status,String msg,Object obj){
        return new AjaxResponse(status,msg,obj);
    }
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getObj() {
        return obj;
    }

    public void setObj(Object obj) {
        this.obj = obj;
    }
}
