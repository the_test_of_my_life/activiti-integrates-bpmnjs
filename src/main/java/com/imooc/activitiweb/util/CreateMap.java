package com.imooc.activitiweb.util;

import java.util.HashMap;
import java.util.Map;

public class CreateMap {

    public static class Build{
        private final Map<String,Object> data = new HashMap<String,Object>();
        public Build setAttribute(String key,Object value){
            data.put(key,value);
            return this;
        }
        public Map<String,Object> build(){
            return data;
        }
    }
}
