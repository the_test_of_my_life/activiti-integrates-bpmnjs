package com.imooc.activitiweb.util;

/**
 * 全局枚举配置类
 */
public class GlobalConfig {

    // 是否是测试环节
    public static final boolean Test = false;

    public enum ResponseCode{
        SUCCESS(0,"成功"),
        ERROR(1,"失败");
        private final int code;
        private final String desc;
        ResponseCode(int code,String desc){
            this.code = code;
            this.desc = desc;
        }
        public int getCode() {
            return code;
        }
        public String getDesc() {
            return desc;
        }
    }
}
