package com.imooc.activitiweb.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class PathMapping implements WebMvcConfigurer {

    @Value("${upload.bpmn.path}")
    private String uploadBpmnPath;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        // 添加默认映射
        registry.addResourceHandler("/**").addResourceLocations("classpath:/resources/");
        // 添加 bpmn 路径映射
        registry.addResourceHandler("/bpmn/**")
                .addResourceLocations("file:"+uploadBpmnPath.replace("/","\\"));
    }
}
