import $ from 'jquery'
const proHost = window.location.protocol + "//" + window.location.host;
const href = window.location.href.split("bpmnjs")[0];
const key = href.split(window.location.host)[1];
const publicurl = proHost + key;
const tools = {
    /**
     * 下载方法
     * @param bpmnModeler
     */
    download(bpmnModeler){
        var downloadLink = $("#downloadBPNM");
        bpmnModeler.saveXML({format:true},function(err,xml){
            if(err){
                return console.error("could not save bpmn",err);
            }
            tools.setEncoded(downloadLink,"digaram.bpmn",xml);
        });
    },
    /**
     * 部署方法
     * @param bpmnModeler
     */
    saveBPMN(bpmnModeler){
        var downloadLink = $("#downloadBPNM");
        bpmnModeler.saveXML({format:true},function(err,xml){
            if(err){
                return console.error("could not save bpmn",err);
            }
            console.info(xml)
            // 参数就是 bpmn xml 字符串
            var param = {
                "xmlBPMN": xml
            };
            // 调用后台接口上传bpmn
            $.ajax({
                url: publicurl + "processDefinition/addDeploymentByString",
                type: "post",
                dataType: "json",
                data: param,
                success: function(res){
                    if(res.status == 0){
                        alert("部署成功");
                    }else{
                        alert("部署失败");
                    }
                },
                error: function(err){
                    console.info(err);
                }
            });
        });
    },
    /**
     * 上传 bpmn
     */
    uploadBPMN(bpmnModeler){
        // 获取文件
        var fileUpload = document.myForm.uploadFile.files[0];
        // 创建 FormData 对象
        var fm = new FormData();
        fm.append("processFile",fileUpload)
        $.ajax({
            url: publicurl + "processDefinition/uploadBPMN",
            type: "post",
            data: fm,
            async: false,
            contentType: false,
            processData: false,
            success: function(res){
                if(res.status == 0){
                    var url = publicurl + "bpmn/" + res.obj;
                    // 打开上传的 bpmn
                    tools.openBPMN_URL(bpmnModeler,url);
                }else{
                    alert(res.msg);
                }
            }
        });
    },
    /**
     * 打开上传的 bpmn
     * @param url
     */
    openBPMN_URL(bpmnModeler,url){
        $.ajax(url,{dataType: "text"}).done(
            // 返回 xml 文件
            async function (xml) {
                try {
                    // 导入 xml
                    await bpmnModeler.importXML(xml);

                }catch (e) {
                    console.error(e);
                }
            });
    },
    /**
     *
     * @param link  下载的按钮
     * @param name  下载的名字
     * @param data  下载的数据
     */
    setEncoded(link, name, data) {
     var encodedData = encodeURIComponent(data);

        if (data) {
            link.addClass('active').attr({
                'href': 'data:application/bpmn20-xml;charset=UTF-8,' + encodedData,
                'download': name
            });
        } else {
            link.removeClass('active');
        }
    }
}

export default tools