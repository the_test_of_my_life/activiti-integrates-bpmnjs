package com.imooc;

import com.imooc.activitiweb.ActivitiwebApplication;
import com.imooc.activitiweb.SecurityUtil;
import org.activiti.api.model.shared.model.VariableInstance;
import org.activiti.api.process.model.ProcessInstance;
import org.activiti.api.process.model.builders.ProcessPayloadBuilder;
import org.activiti.api.process.runtime.ProcessRuntime;
import org.activiti.api.runtime.shared.query.Page;
import org.activiti.api.runtime.shared.query.Pageable;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Deployment;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * ProcessRuntime 新特性测试
 */
@SpringBootTest(classes = ActivitiwebApplication.class)
public class Part10_ProcessRuntime {

    @Autowired
    private ProcessRuntime processRuntime;
    @Autowired
    private SecurityUtil securityUtil;
    @Autowired
    private RepositoryService repositoryService;

    // 获取流程实例
    @Test
    public void getProcessInstance(){
        // 登录 这个 securityUtil 是我们复制下来的，不是 Apache 下的
        securityUtil.logInAs("zhangsan");
        // 查询
        Page<ProcessInstance> pdp = processRuntime
                .processInstances(Pageable.of(0, 100));
        System.out.println("流程实例总数量："+pdp.getTotalItems());
        // 查询流程实例
        List<ProcessInstance> pis = pdp.getContent();
        pis.forEach(p -> {
            System.out.println("流程id = "+p.getId());
            System.out.println("流程名称 = "+p.getName());
            System.out.println("开始日期 = "+p.getStartDate());
            System.out.println("状态 = "+p.getStartDate());
            System.out.println("流程定义id = "+p.getProcessDefinitionId());
            System.out.println("流程定义key = "+p.getProcessDefinitionKey());
        });
    }

    /**
     * 部署流程还是用以前的方式，新的 api 中没有封装
     */
    @Test
    public void deployProcessInstance(){
        Deployment deploy = repositoryService.createDeployment()
                .disableSchemaValidation()
                .addClasspathResource("BPMN/part10_processRuntime.bpmn")
                .deploy();
        System.out.println("流程部署 id = " + deploy.getId());
    }

    // 启动流程实例
    @Test
    public void startProcessInstance(){
        // 登录
        securityUtil.logInAs("zhangsan");
        processRuntime.start(ProcessPayloadBuilder
        .start()
        .withProcessDefinitionKey("myProcess_processRuntime")  // 根据流程 key 启动
//        .withVariable("aaa","bb" )
//        .withVariables("map")
        .withBusinessKey("可以自定义业务 key")
        .build()
        );
    }

    // 删除流程实例
    @Test
    public void delProcessInstance(){
        // 登录
        securityUtil.logInAs("zhangsan");
        processRuntime.delete(ProcessPayloadBuilder
        .delete()
        .withProcessInstanceId("edb14933-2f06-11eb-b202-9c5c8e79c1e8")
        .build()
        );
    }

    // 挂起流程
    @Test
    public void suspendProcessInstance(){
        // 登录
        securityUtil.logInAs("zhangsan");
        processRuntime.suspend(ProcessPayloadBuilder
        .suspend()
        .withProcessInstanceId("流程实例 id")
        .build()
        );
    }

    // 激活流程实例
    @Test
    public void resumeProcessInstance(){
        // 登录
        securityUtil.logInAs("zhangsan");
        processRuntime.resume(ProcessPayloadBuilder
        .resume()
        .withProcessInstanceId("流程实例 id")
        .build()
        );
    }

    // 流程实例参数
    @Test
    public void getVariables(){
        // 登录
        securityUtil.logInAs("zhangsan");
        List<VariableInstance> variables = processRuntime.variables(ProcessPayloadBuilder
                .variables()
                .withProcessInstanceId("d6e3277d-2ed3-11eb-8d1b-9c5c8e79c1e8")
                .build()
        );
        variables.forEach(var -> {
            System.out.println("变量名称="+var.getName());
            System.out.println("变量值="+var.getValue());
            System.out.println("任务id="+var.getTaskId());
            System.out.println("流程id="+var.getProcessInstanceId());
        });
    }
}
