package com.imooc;

import com.imooc.activitiweb.ActivitiwebApplication;
import com.imooc.activitiweb.SecurityUtil;
import org.activiti.api.runtime.shared.query.Page;
import org.activiti.api.runtime.shared.query.Pageable;
import org.activiti.api.task.model.Task;
import org.activiti.api.task.model.builders.TaskPayloadBuilder;
import org.activiti.api.task.runtime.TaskRuntime;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * TaskRuntime 测试
 */
@SpringBootTest(classes = ActivitiwebApplication.class)
public class Part11_taskRuntime {

    @Autowired
    private SecurityUtil securityUtil;
    @Autowired
    private TaskRuntime taskRuntime;

    /**
     * 获取当前登录用户任务
     */
    @Test
    public void getTasks(){
        securityUtil.logInAs("zhangsan");
        // 查询 0 - 100 条
        Page<Task> tasks = taskRuntime.tasks(Pageable.of(0, 10));
        List<Task> list = tasks.getContent();
        // taskRuntime 其实是将 执行人和候选人都查出来了
        list.forEach(t -> {
            System.out.println("任务id="+t.getId());
            System.out.println("任务名称="+t.getName());
            System.out.println("任务状态="+t.getStatus());
            System.out.println("创建时间="+t.getCreatedDate());
            // taskRuntime 其实是将 执行人和候选人都查出来了
            // 我们可以在这里做一个判断
            // 如果 assignee 执行人能查出来，但是如果等于 null ,说明某一个任务的候选人有他，需要当前用户去拾取任务
            if(t.getAssignee() == null){
                // 说明该任务的候选人有他，需要拾取
                System.out.println("assignee: 这是一条待拾取的任务");
            }else{
                System.out.println("执行人就是当前用户 assignee: "+t.getAssignee());
            }
        });
    }

    /**
     * 完成任务
     */
    @Test
    public void completeTask(){
        // 登录
        securityUtil.logInAs("zhangsan");
        // 执行任务
        Task task = taskRuntime.task("任务 id");
        // 如果执行人为 null 说明当前执行任务是候选人，需要先拾取任务
        if(task.getAssignee() == null){
            // 拾取任务
            taskRuntime.claim(TaskPayloadBuilder
            .claim()
            .withTaskId(task.getId())
            .build()
            );
        }
        // 执行任务
        taskRuntime.complete(TaskPayloadBuilder
        .complete()
        .withTaskId(task.getId())
        .build()
        );
        System.out.println("任务执行完成");
    }
}

