package com.imooc;

import com.imooc.activitiweb.ActivitiwebApplication;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.ProcessDefinition;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest(classes = ActivitiwebApplication.class)
public class Part2_processDefinition {

    @Autowired
    private RepositoryService repositoryService;

    // 查询流程定义
    @Test
    public void getDefinition(){
        List<ProcessDefinition> list = repositoryService.createProcessDefinitionQuery().list();
        for (ProcessDefinition pd : list) {
            System.out.println("name ：" + pd.getName());
            System.out.println("key ：" + pd.getKey());
            System.out.println("name ：" + pd.getResourceName());
            System.out.println("deploymentId ：" + pd.getDeploymentId());
            System.out.println("version ：" + pd.getVersion());
        }
    }

    /**
     * 删除流程定义
     */
    @Test
    public void delDefinition(){
        String pid = "f434aaf6-28eb-11eb-8d4d-408d5c97c1d5";
        // 参数一： 流程定义id
        // 参数二： true 表示将删除该路程下所有的流程任务及历史
        //          false 不会删除任务及历史
        repositoryService.deleteDeployment(pid, false);
    }
}
