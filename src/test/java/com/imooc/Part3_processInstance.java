package com.imooc;

import com.imooc.activitiweb.ActivitiwebApplication;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.runtime.ProcessInstance;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * 流程实例
 */
@SpringBootTest(classes = ActivitiwebApplication.class)
public class Part3_processInstance {

    @Autowired
    private RuntimeService runtimeService;

    /**
     * 初始化流程实例
     */
    @Test
    public void initProcessInstance(){
        /**
         * 参数1  流程定义 key
         * 参数2  业务 businessKey 一般是和自己的业务进行关联的主键 id ，也可以建立一张中间表，
         *        将流程实例和业务关联起来，由于实例中有 businessKey 这里直接使用就可以了
         *
         * 通常的做法：
         *      1. 获取页面表单数据
         *      2. 写入业务表，返回业务主键 id
         *      3. 把业务 id 和 流程数据关联起来
         *
         *      * 影响的表
         *      *  act_hi_actinst      已完成的活动信息
         *      *  act_hi_identitylink 参与者信息
         *      *  act_hi_procinst     流程实例
         *      *  act_hi_taskinst     任务实例
         *      *  act_ru_execution    执行表
         *      *  act_ru_identitylink 参与者信息
         *      *  act_ru_task         任务
         *
         */
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("myProcess_10", "myProcess_10");
        System.out.println("流程实例 id = " + processInstance.getId());
    }

    /**
     * 获取流程实例
     */
    @Test
    public void getProcessInstances(){
        List<ProcessInstance> list = runtimeService.createProcessInstanceQuery().list();
        for (ProcessInstance pi : list){
            System.out.println("流程实例id = "+pi.getProcessInstanceId());
            System.out.println("流程定义id = "+pi.getProcessDefinitionId());
            System.out.println("流程是否完成 = "+pi.isEnded());
            System.out.println("流程是否挂起 = "+pi.isSuspended());
        }
    }

    /**
     * 暂停流程实例
     */
    @Test
    public void suspendProcessInstance(){

        runtimeService.suspendProcessInstanceById("4d0f2a47-2a7d-11eb-a730-408d5c97c1d5");
        System.out.println("挂起流程实例");
    }

    /**
     * 激活流程实例
     */
    @Test
    public void activeProcessInstance(){
        runtimeService.activateProcessInstanceById("4d0f2a47-2a7d-11eb-a730-408d5c97c1d5");
        System.out.println("激活流程实例");
    }

    /**
     * 删除流程实例
     */
    @Test
    public void deleteProcessInstance(){
        runtimeService.deleteProcessInstance("4d0f2a47-2a7d-11eb-a730-408d5c97c1d5","不想要了...");
        System.out.println("删除成功");
    }
}
