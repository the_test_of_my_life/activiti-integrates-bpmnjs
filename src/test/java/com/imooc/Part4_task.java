package com.imooc;

import com.imooc.activitiweb.ActivitiwebApplication;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest(classes = ActivitiwebApplication.class)
public class Part4_task {

    @Autowired
    private TaskService taskService;

    /**
     * 查询所有任务
     */
    @Test
    public void getTasks(){
        List<Task> list = taskService.createTaskQuery().list();
        for (Task task : list) {
            System.out.println("任务名称：" + task.getName());
            System.out.println("任务执行人：" + task.getAssignee());
            System.out.println("流程实例id：" + task.getProcessInstanceId());
        }
    }

    /**
     * 查询我的待办任务
     */
    @Test
    public void getTaskByAssignee(){
        List<Task> zhangsanTask = taskService.createTaskQuery()
                .taskAssignee("张三")
                .list();
        for (Task task : zhangsanTask) {
            System.out.println("任务id：" + task.getId());
            System.out.println("任务名称：" + task.getName());
            System.out.println("任务执行人：" + task.getAssignee());
            System.out.println("流程实例id：" + task.getProcessInstanceId());
        }
    }

    /**
     * 执行任务
     * 影响的表   act_run_task   act_hi_taskinst
     */
    @Test
    public void completeTask(){
        taskService.complete("ba9d7307-2c18-11eb-bc8d-408d5c97c1d5");
        System.out.println("执行任务");
    }

    //拾取任务
    @Test
    public void claimTask(){
        Task task = taskService.createTaskQuery().taskId("1f2a8edf-cefa-11ea-84aa-dcfb4875e032").singleResult();
        taskService.claim("1f2a8edf-cefa-11ea-84aa-dcfb4875e032","bajie");
    }

    //归还与交办任务
    @Test
    public void setTaskAssignee(){
        Task task = taskService.createTaskQuery().taskId("1f2a8edf-cefa-11ea-84aa-dcfb4875e032").singleResult();
        taskService.setAssignee("1f2a8edf-cefa-11ea-84aa-dcfb4875e032","null");//归还候选任务
        taskService.setAssignee("1f2a8edf-cefa-11ea-84aa-dcfb4875e032","wukong");//交办
    }
}
