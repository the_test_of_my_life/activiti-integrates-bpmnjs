package com.imooc;


import com.imooc.activitiweb.ActivitiwebApplication;
import org.activiti.engine.HistoryService;
import org.activiti.engine.history.HistoricTaskInstance;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * 流程任务历史
 */
@SpringBootTest(classes = ActivitiwebApplication.class)
public class Part5_histroyTaskInstance {

    @Autowired
    private HistoryService historyService;

    /**
     * 根据用户名查询历史记录
     */
    @Test
    public void historyTaskInstanceByUser(){
        List<HistoricTaskInstance> history = historyService.createHistoricTaskInstanceQuery()
                .orderByHistoricTaskInstanceEndTime()
                .asc()
                .taskAssignee("张三")
                .list();
        for (HistoricTaskInstance hi : history){
            System.out.println("执行人：" + hi.getAssignee());
            System.out.println("name：" + hi.getName());
            System.out.println("流程实例id：" + hi.getProcessInstanceId());
        }
    }

    /**
     *  根据流程实例查询任务
     */
    @Test
    public void historyTaskInstanceByProcessInstanceId(){
        List<HistoricTaskInstance> list = historyService.createHistoricTaskInstanceQuery()
                .orderByHistoricTaskInstanceEndTime().asc()
                .processInstanceId("f5a091d9-2a85-11eb-be00-408d5c97c1d5")
                .list();
        for (HistoricTaskInstance hi : list){
            System.out.println("执行人：" + hi.getAssignee());
            System.out.println("name：" + hi.getName());
            System.out.println("流程实例id：" + hi.getProcessInstanceId());
        }
    }
}
