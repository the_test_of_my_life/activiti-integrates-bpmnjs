package com.imooc;

import com.imooc.activitiweb.ActivitiwebApplication;
import com.imooc.pojo.User;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.Map;

@SpringBootTest(classes = ActivitiwebApplication.class)
public class Part6_UEL {
    
    @Autowired
    private RepositoryService repositoryService;
    
    @Autowired
    private RuntimeService runtimeService;
    
    @Autowired
    private TaskService taskService;

    /**
     * 部署流程实例
     */
    @Test
    public void deplyProcessInstance(){
        Deployment deploy = repositoryService
                .createDeployment()
                .disableSchemaValidation()
                .addClasspathResource("BPMN/Part6_UEL_v3.bpmn")
                .deploy();
        System.out.println("部署id：" + deploy.getId());
        System.out.println("部署名称：" + deploy.getName());
        System.out.println("部署版本：" + deploy.getVersion());
    }

    /**
     * 启动流程实例带参数
     */
    @Test
    public void initProcessInstanceWithArgs(){
        // 流程变量
        Map<String,Object> variables = new HashMap<String,Object>();
        variables.put("assignee","张一山");
        ProcessInstance processInstance = runtimeService
                .startProcessInstanceByKey("myProcess_UEL", variables);
        System.out.println("流程实例id：" + processInstance.getProcessDefinitionId());
    }

    /**
     * UEL 测试
     *  费用报销单，小于，等于 100 经理审批，大于 100 主管审批
     */
    @Test
    public void completeTaskWithArgs(){
        Map<String,Object> variables = new HashMap<String,Object>();
        variables.put("money", 101);
        taskService.complete("7ec08949-2c13-11eb-b4b1-408d5c97c1d5",variables);
        System.out.println("大于 100 应该是主管审批");
    }

    /**
     * 启动流程实例，对象参数
     */
    @Test
    public void initProcessInstanceWithClassArgs(){
        User user = new User();
        // 设置执行人
        user.setAssign("zhangsan");
        // 设置候选人
        user.setCandidate("lisi,wangwu");
        Map<String,Object> variables = new HashMap<String,Object>();
        variables.put("user",user);
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("myProcess_11", variables);
        System.out.println("流程实例 id" + processInstance.getProcessDefinitionId());
    }

    /**
     * 拾取任务
     */
    @Test
    public void claimTask(){
        taskService.claim("95152f81-2c19-11eb-b715-408d5c97c1d5","fdf");
        System.out.println("拾取任务成功");
    }

    /**
     * 直接指定全局流程变量
     */
    @Test
    public void setVariables(){
        // 通过 id 设置流程变量
        // 参数1：流程实例id
        // 参数2：流程变量名
        // 参数3：流程变量所对应的值

        // 设置单个变量
        runtimeService.setVariable("2501","name","zhangsan");

        Map<String,Object> variables = new HashMap<String,Object>();
        variables.put("name","zhangsan");
        variables.put("age","15");
        // 设置多个变量
        runtimeService.setVariables("2501",variables);

        // 通过任务节点 id 设置流程变量   同上
//        taskService.setVariables();
//        taskService.setVariable();
    }

    /**
     * 设置局部变量，只在当前环节有用
     */
    @Test
    public void setLocalVariable(){
        /*runtimeService.setVariableLocal();
        runtimeService.setVariablesLocal();
        taskService.setVariableLocal();
        taskService.setVariablesLocal();*/
    }
}
