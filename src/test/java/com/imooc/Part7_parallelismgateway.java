package com.imooc;

import com.imooc.activitiweb.ActivitiwebApplication;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = ActivitiwebApplication.class)
public class Part7_parallelismgateway {

    @Autowired
    RepositoryService repositoryService;
    @Autowired
    private TaskService taskService;
    @Autowired
    private RuntimeService runtimeService;

    /**
     * 部署流程
     */
    @Test
    public void deployProcess(){
        Deployment deploy = repositoryService.createDeployment()
                .disableSchemaValidation()
                .addClasspathResource("BPMN/part7_parallelism.bpmn")
                .deploy();
        System.out.println("部署 id = " + deploy.getId());
    }

    /**
     * 启动流程实例
     */
    @Test
    public void startProcessInstance(){
        ProcessInstance pi = runtimeService.startProcessInstanceByKey("myProcess_holiday");
        System.out.println("流程实例 id = " + pi.getId());
        System.out.println("流程名称 = " + pi.getName());
    }

    /**
     * zhangsan 执行任务
     */
    @Test
    public void execTask(){
        taskService.complete("adc03b8a-2ca3-11eb-b95d-005056c00008");
        System.out.println("zhangsan 填写请假单");
    }
}
