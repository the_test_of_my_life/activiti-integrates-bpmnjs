package com.imooc;

import com.imooc.activitiweb.ActivitiwebApplication;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.Map;

/**
 *
 */
@SpringBootTest(classes = ActivitiwebApplication.class)
public class Part8_exclusivegateway {

    @Autowired
    private RepositoryService repositoryService;
    @Autowired
    private RuntimeService runtimeService;
    @Autowired
    private TaskService taskService;

    /**
     * 部署流程
     */
    @Test
    public void deployProcess(){
        Deployment deploy = repositoryService.createDeployment()
                .disableSchemaValidation()
                .addClasspathResource("BPMN/part8_exclusive.bpmn")
                .deploy();
        System.out.println("流程部署 id = " + deploy.getId());
    }

    /**
     * 启动流程实例
     */
    @Test
    public void startProcessInstance(){
        ProcessInstance pi = runtimeService.startProcessInstanceByKey("myProcess_exclusive");
        System.out.println("流程实例 id = " + pi.getId());
        System.out.println("流程实例名称：" + pi.getName());
    }

    /**
     * zhangsan 执行请假任务  请假10 天  大于 3 天 应该是 wangwu 审核
     */
    @Test
    public void execTask(){
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("day",10);
        taskService.complete("d6eac7a1-2ed3-11eb-8d1b-9c5c8e79c1e8", map);
        System.out.println("zhansan 执行请假任务");
    }
}
