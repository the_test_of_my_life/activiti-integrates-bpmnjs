package com.imooc;

import com.imooc.activitiweb.ActivitiwebApplication;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.Map;

/**
 * 包含网关
 */
@SpringBootTest(classes = ActivitiwebApplication.class)
public class Part9_inclusivegateway {

    @Autowired
    private RepositoryService repositoryService;
    @Autowired
    private RuntimeService runtimeService;
    @Autowired
    private TaskService taskService;

    /**
     * 流程部署
     */
    @Test
    public void deployProcess(){
        Deployment deploy = repositoryService.createDeployment()
                .disableSchemaValidation()
                .addClasspathResource("BPMN/part9_inclusive.bpmn")
                .deploy();
        System.out.println("流程部署 id = " + deploy.getId());
    }

    /**
     * 启动流程
     */
    @Test
    public void startProcess(){
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("myProcess_inclusive");
        System.out.println("流程实例 id = " + processInstance.getId());
    }

    /**
     * zhangsan 执行任务 请假一天
     */
    @Test
    public void execTask(){
        // 流程变量
        Map<String,Object> map = new HashMap<String,Object>();
        // 请假一天
        map.put("day", 1);
        taskService.complete("3c1c7f0a-2ee9-11eb-94e6-9c5c8e79c1e8",map);
        System.out.println("张三填写请假单");
    }

    /**
     * 王五赵六审批任务
     */
    @Test
    public void auditTask(){
        taskService.complete("8c78e4ba-2eea-11eb-bd03-9c5c8e79c1e8");
        taskService.complete("8c790bcc-2eea-11eb-bd03-9c5c8e79c1e8");
        System.out.println("wangwu zhaoliu 审批");
    }
}
