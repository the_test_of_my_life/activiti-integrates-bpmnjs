package com.imooc;

import com.imooc.activitiweb.ActivitiwebApplication;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Deployment;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.InputStream;
import java.util.List;
import java.util.zip.ZipInputStream;

@SpringBootTest(classes = ActivitiwebApplication.class)
public class part1_deployment {

    @Autowired
    private RepositoryService repositoryService;

    /**
     * 流程部署测试 1
     *  影响的表
     *      act_re_procdef       流程定义表
     *      act_re_deployment    流程部署表
     *      act_ge_bytearray     通用的流程定义和流程资源
     *
     *      ge 代表  general     通用的
     *      re 代表  repository
     */
    @Test
    public void initDeploymentBPMN(){
        String fileName= "BPMN/part1_deployment.bpmn";
        String imageName = "BPMN/part1_deployment.png";
        Deployment deployment = repositoryService.createDeployment()
                .disableSchemaValidation()
                .addClasspathResource(fileName)
                .addClasspathResource(imageName)
                .name("部署流程测试_v1")
                .deploy();
        System.out.println(deployment.getName());
    }

    /**
     * 流程部署测试 zip
     */
    @Test
    public void initDeploymentZIP(){
        InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream("BPMN/part1_deployment.zip");
        ZipInputStream zip = new ZipInputStream(resourceAsStream);
        Deployment deployment = repositoryService.createDeployment()
                .addZipInputStream(zip)
                .name("流程部署测试_zip")
                .deploy();
        System.out.println(deployment.getName());
    }

    /**
     * 查询部署流程列表
     */
    @Test
    public void getDeployments(){
        List<Deployment> list = repositoryService.createDeploymentQuery().list();
        list.forEach(deployment -> {
            System.out.println("id: "+ deployment.getId());
            System.out.println("name: " + deployment.getName());
            System.out.println("deploymentTime: " + deployment.getDeploymentTime());
            System.out.println("key: " + deployment.getKey());
        });
    }
}
