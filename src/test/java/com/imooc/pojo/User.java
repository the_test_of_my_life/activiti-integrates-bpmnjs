package com.imooc.pojo;

import java.io.Serializable;

/**
 * 注意要实现 serializable 接口
 * 变量名要小写
 */
public class User implements Serializable {

    // 执行人
    private String assign;
    // 候选人
    private String candidate;

    public String getAssign() {
        return assign;
    }

    public void setAssign(String assign) {
        this.assign = assign;
    }

    public String getCandidate() {
        return candidate;
    }

    public void setCandidate(String candidate) {
        this.candidate = candidate;
    }
}
